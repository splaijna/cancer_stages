library(shiny)

ui <- fluidPage(

  titlePanel("Data Processing and Classification"), 
   sidebarLayout(
    sidebarPanel(
      inputfile<-actionButton("MyFile", "Upload Data" ),
      tags$hr(),

      checkboxInput("TrainTest", "Show Train Data", FALSE),
     selectInput("variable1", "Variable_1:",
                 names(inputfile)),

     selectInput("variable2", "Variable_2:",
                 (names(inputfile))),
 
      numericInput("obs", "Number of observations to view:", 50, min=1, max=200),
      br(),

      radioButtons("method", "Classification method:",
                   c("K-Means" = "kmeans",
                     "LDA" = "lda"))
      ),
    
    mainPanel(
      tabsetPanel(type = "tabs",
                  tabPanel("Summary", verbatimTextOutput("summary")),
                  tabPanel("Table", tableOutput("table")),
                  tabPanel("Plot", plotOutput(outputId ="plotbox"),
                                   plotOutput(outputId ="plotbox2"), 
                                   plotOutput(outputId = "plotvalue"),
                                   plotOutput(outputId = "plotvalue2"),
                                   plotOutput(outputId = "plothist")),
                  tabPanel("ClassifRezult", verbatimTextOutput("classific"))
      )

    )
  )
)

server <- function(input, output, session) {

  LoadF <- eventReactive(input$MyFile,
    {
 
      if(input$MyFile==0)
        return(NULL)
    
      My_URL_File <- read.csv('https://gitlab.com/splaijna/cancer_stages/raw/master/ProstCanc4st.csv', sep = ";", dec = ",")
      save(My_URL_File, file="My_URL_File.RData")
      load("./My_URL_File.RData", MyFile <- new.env())
      get(ls.str(MyFile)) 
      
    })
  
 
  # Return the requested LoadF

  datasetInput <- reactive( {
    datafile<-LoadF() 
    
    library(dplyr)
    library(lattice)
    library(ggplot2)
    library(caret)
    
    updateSelectInput(session, inputId = "variable1", choices = names(datafile))
    updateSelectInput(session, inputId = "variable2", choices = names(datafile))
    
   
    #updateNumericInput(session, inputId = "obs", max= nrow(datafile))# max= 
    
    
    
      if(input$TrainTest) 
      {
        index <- createDataPartition(datafile$Group, p=0.80, list=FALSE)
        # select 80% of data to train the models
        trainset <- datafile[index,]
       return(trainset)
      }
  else 
  {
    return(datafile)
  }
  
 
  })#, ignoreNULL = FALSE
  

  output$summary <- renderPrint(
    {
    
      dataset<-datasetInput()
      summary(dataset)
    }
  )

 
  
  output$table <- renderTable({

    dataset<-datasetInput()
    
    updateNumericInput(session, "obs", max = nrow(dataset))
    #if(input$obs)
     return (head(datasetInput(), n = input$obs))
            #  else
            # {  return(0)}

  })
  

  
   
  # Generate a plot of the data
  output$plotbox <- renderPlot({
    
    dataset<-datasetInput()
    par(mfrow=c(2,2)) 
    for(i in 2:5) {
      boxplot(dataset[,i]~dataset$Group, varwidth = T,main=names(dataset)[i])
    }
    
  })
  
  output$plotbox2 <- renderPlot({
    
    dataset<-datasetInput()
    ## Box Plot
    box <- ggplot(data=dataset, aes(x=Group, y=dataset[[input$variable2]])) +
      geom_boxplot(aes(fill=Group)) + 
      ylab(input$variable2) +
      ggtitle("Pacients Boxplot") +
      stat_summary(fun.y=mean, geom="point", shape=5, size=3) 
    print(box)
    
  })
  
  output$plotvalue <- renderPlot({
    
    dataset<-datasetInput()
    ## Plot of 2 values by Groups
    g <- ggplot(data=dataset, aes(x = dataset[[input$variable1]], y = dataset[[input$variable2]]))
    g <-g + 
      geom_point(aes(color=Group, shape=Group)) +
      xlab(input$variable1) +
      ylab(input$variable2) +
      ggtitle("Plot of 2 values by Groups")+
      geom_smooth(method="lm")
    print(g)
    
  })
 

  
  output$plotvalue2 <- renderPlot({
    
    dataset<-datasetInput()
  ## Faceting: Producing multiple charts in one plot
  library(ggthemes)
  facet <- ggplot(data=dataset, aes(x=dataset[[input$variable1]], y=dataset[[input$variable2]], color=Group))+
    geom_point(aes(shape=Group), size=1.5) + 
    geom_smooth(method="lm") +
    xlab(input$variable1) +
    ylab(input$variable2) +
    ggtitle("Compare in each group") +
    theme_fivethirtyeight() +
    facet_wrap(. ~ Group) # Along 2 rows
  print(facet)
  
  })
  
  
  output$plothist <- renderPlot({
    
    dataset<-datasetInput()
    
    library(ggthemes)
    ## Histogram
    
    histogram <- ggplot(data=dataset, aes(x=dataset[[input$variable1]])) +
      geom_histogram(binwidth=1, color="black", aes(fill=Group)) + 
      xlab(input$variable1) +  
      ylab("Frequency") + 
      ggtitle("Histogram of input_variable1")+
      theme_economist()
    print(histogram)
  })
  
  
  
  output$classific <- reactive({
    
   #Classif_KL_Meth()
    method<<- switch (input$method,
                     kmeans = Classif_KM_Meth(),
                     lda = Classif_KL_Meth()
    )
 
  }) 
  
  
  output$classific <- renderPrint({
    datafile<-LoadF()
    
    # We use the LoadF to create a partition (80% training 20% testing)
    index <- createDataPartition(datafile$Group, p=0.80, list=FALSE)
    # select 20% of the data for testing
    testset  <- datafile[-index,]
    # select 80% of data to train the models
    trainset <- datafile[index,]
    
    Classif_KM_Meth <<- function()
    {
      
      ## Since Kmeans is a random start algo, 
      ## we need to set the seed to ensure reproduceability
      set.seed(80)
      PacientCluster <- kmeans(datafile[, 2:25], centers = 4, nstart = 80)
      PacientCluster
      ClustPac<<-table(PacientCluster$cluster, datafile$Group)
      return(ClustPac)
      
    }
    
    Classif_KL_Meth <<- function()
    {
      
      ## Linear Discriminant Analysis
      
      library(caret)
      #install.packages("MASS")
      library(MASS)
      library(e1071)
      set.seed(1000)
      # Fit the model
      model.lda<-train(x = trainset[,2:25],y = trainset[,26], method = "lda",metric = "Accuracy")
      
      # Print the model
      print("2) LDA model classification")
      Lda_Meth<-print(model.lda)
      
      
      ## Verify the accuracy on the training set
      pred<-predict(object = model.lda,newdata = trainset[,2:25])
      PredTrain<-confusionMatrix(pred,trainset$Group)
      
      print("3) Verify the accuracy on the training set")
      print(PredTrain)
      
      ## Performance on the test set
      pred_test<-predict(object = model.lda,newdata = testset[,2:25])
      PredTest<<-confusionMatrix(pred_test,testset$Group)
      return(PredTest)
      
      
      
    }
    
    if (input$method == "kmeans" ) {Classif_KM_Meth()
      print("1) K-means model classification")
      print(ClustPac)
      
    } else {Classif_KL_Meth()
      print("4) Performance on the test set")
      print(PredTest)
      
      }
    
  
  
  })
  
  
}

# Create Shiny app ----
shinyApp(ui, server)
